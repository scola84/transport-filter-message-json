'use strict';

const Error = require('@scola/error');

class JsonFilter {
  receive(message) {
    return new Promise((resolve, reject) => {
      try {
        resolve(message.setBody(
          JSON.parse(message.getBody())
        ));
      } catch (error) {
        reject(new Error('json_parse_error', {
          origin: error,
          detail: {
            message
          }
        }));
      }
    });
  }

  send(message) {
    return new Promise((resolve, reject) => {
      try {
        resolve(message.setBody(
          JSON.stringify(message.getBody())
        ));
      } catch (error) {
        reject(new Error('json_stringify_error', {
          origin: error,
          detail: {
            message
          }
        }));
      }
    });
  }
}

module.exports = JsonFilter;
